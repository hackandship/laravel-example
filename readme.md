## Laravel on Docker

Attempt to serve a laravel app on docker using 2 different stacks:
- Stack 1: mysql and phpmyadmin service that can be shared by different apps
- Stack 2: nginx and php services (app stack) 

### Clone

git clone git@github.com:TechyTimo/docker-shared-stacks.git

### Setup phpmyadmin stack

cd ../phpmyadmin
docker-compose up

### Setup the laravel app stack

cd docker-shared-stacks/laravel
cp .env.example .env

### Edit the .env file
  DB_DATABASE
  DB_USERNAME
  DB_PASSWORD


docker-compose up
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate

### Test

1. Access laravel app at http://localhost:8000
2. View the database connection http://localhost:8000/connection
3. Register an account at http://localhost:8000/register
4. View the registered user http://localhost:8000/user
5. Access phpmyadmin at http://localhost:8080
6. Log in with


